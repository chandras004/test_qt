#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

  //  QLabel *label;

    void somefunctionThatChangesText(const QString& newtext)
    {
        emit changeTextSignal(newtext);
    }

    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:

void changeTextSignal(QString);

private slots:
    void on_pushButton_clicked();

  //  void on_pushButton_clicked(bool checked);
  // void on_pushButton_released();

private:
    Ui::MainWindow *ui;
    QLabel *label;
};

#endif // MAINWINDOW_H
